define([
  './physicsjs/physicsjs-full',
  './pixi',
  './story',
  './position'
], function (Physics, PIXI, Story, PositionManager) {

  return function () {

    window.objectsOnStage = [];
    var gravity = null,
      world = null,
      lastLoop = 0,
      avgFPS = 0,
      FPScounts = 0,
      totalFPS = 0;

    function otherStuff() {
      window.objectsOnStage.forEach(function (obj) {
        if (obj.object.update) {
          obj.object.update();
        }
      });
    }

    function updateFPS() {
      var thisLoop = new Date;
      var fps = 1000 / (thisLoop - lastLoop);
      FPScounts++;
      totalFPS += fps;
      avgFPS = totalFPS / FPScounts;
      lastLoop = thisLoop;
      //$('#debug').text(parseInt(avgFPS) + ' fps');
    }

    function initWorld(_world) {
      window.P = Physics;
      world = _world;
      window.PIXI = PIXI;

      var viewWidth = $(document).width(),
        viewHeight = $(document).height(),
        renderer = Physics.renderer('pixi', {
          el: 'layer2',
          width: viewWidth,
          height: viewHeight,
          meta: false,
          resolution: 1
        }),
        viewportBounds = Physics.aabb(0, 0, viewWidth, viewHeight);

      world.add(renderer);

      world.add(Physics.behavior('edge-collision-detection', {
        aabb: viewportBounds,
        restitution: 0.5,
        cof: 0.1
      }));

      world.add(Physics.behavior('body-impulse-response'));
      world.add(Physics.behavior('body-collision-detection'));

      gravity = Physics.behavior('constant-acceleration');
      gravity.setAcceleration({x: 0, y: -1});
      //world.add( gravity );

      //window.addEventListener('deviceorientation', onDeviceOrientation, false);

      world.add(Physics.behavior('sweep-prune'));
      world.add(Physics.behavior('interactive', {el: renderer.el}));

      Physics.util.ticker.on(function (time, dt) {
        world.step(time);

        otherStuff();
        updateFPS();
      });

      Physics.util.ticker.start();

      var positionManager = new PositionManager([viewWidth, viewHeight]),
        story = new Story(renderer, world, Physics, $('#viewport'), positionManager);

      story.begin();

      world.on('step', function () {
        world.render();
        story.doStep();
      });

      $('canvas').css({
        width: viewWidth,
        height: viewHeight
      });
    }

    function run(Physics, PIXI) {
      Physics({
        maxIPF: 2,
        sleepDisabled: true
      }, initWorld);
    }

    run(Physics, PIXI);

    var hideInterval = null;
    $(window).mousemove(function () {
      $('#quit').show();
      if (hideInterval) {
        clearTimeout(hideInterval);
      }
      hideInterval = setTimeout(function () {
        $('#quit').hide();
      }, 2000);
    });
  }
});
