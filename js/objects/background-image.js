define([], function () {
  return function (options, renderer, world) {

    this.init = function () {
      var that = this;
      this.alive = true;
      this.dying = false;
      this.loaded = false;
      this.scaleSpeed = options.nozoom ? 1 : 1.00005 + Math.random() / 1200;

      //var bgTex = PIXI.Texture.fromImage(options.src);

      var loader = new PIXI.loaders.Loader();
      loader.add('test', options.src);
      loader.once("complete", onComplete);
      loader.load();


      function onComplete(e) {
        var blendMode = options.blend || 0;
        that.bgImg = new PIXI.Sprite(e.resources['test'].texture);
        that.bgImg.alpha = 0;


        if (blendMode != 0 || options.top) {
          renderer.stage.addChild(that.bgImg);
        } else {
          renderer.stage.addChildAt(that.bgImg, 0);
        }

        var imageRatio = that.bgImg.width / that.bgImg.height,
          screenRatio = renderer.width / renderer.height;

        if (imageRatio < screenRatio) {
          that.bgImg.width = renderer.width;
          that.bgImg.height = renderer.width / imageRatio;
        } else {
          that.bgImg.height = renderer.height;
          that.bgImg.width = renderer.height * imageRatio;
        }

        that.bgImg.x = that.bgImg.width / 2 - Math.abs(renderer.width - that.bgImg.width) / 2;
        that.bgImg.y = that.bgImg.height / 2 - Math.abs(renderer.height - that.bgImg.height) / 2;

        that.bgImg.blendMode = blendMode;
        that.bgImg.anchor = {
          x: 0.5,
          y: 0.5
        };

        that.loaded = true;
      }
    };

    this.suicide = function () {
      this.dying = true;
    };

    this.update = function () {
      if (this.loaded) {
        this.bgImg.scale.x *= this.scaleSpeed;
        this.bgImg.scale.y = this.bgImg.scale.x;
      }

    };

    this.doStep = function () {
      var fadeSpeed = options.sudden ? 0.01 : 0.001;


      if (!this.bgImg) return;
      if (options.dr) {
        this.bgImg.rotation += options.dr;
      }

      if (!this.dying) {
        if (options.nofade) {
          this.bgImg.alpha = 1;
        } else {
          if (this.bgImg.alpha < 1) {
            this.bgImg.alpha += fadeSpeed;
            this.bgImg.alpha = Math.min(1, this.bgImg.alpha);
          }
        }

      } else {
        //this.bgImg.alpha = 0;
        if (options.nofade) {
          this.bgImg.alpha = 0;
          renderer.stage.removeChild(this.bgImg);
          this.alive = false;
        } else {
          if (this.bgImg.alpha > 0) {
            this.bgImg.alpha -= fadeSpeed;
            this.bgImg.alpha = Math.max(0, this.bgImg.alpha);
            if (this.bgImg.alpha <= 0) {
              renderer.stage.removeChild(this.bgImg);
              this.alive = false;
            }
          }
          /*this.bgImg.alpha *= 0.975;
           */
        }

      }
    };

    this.init();
  }
});