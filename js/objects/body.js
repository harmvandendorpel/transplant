define([], function (){
  return function (options, world, Physics, positionManager) {
      this.init = function () {
          var that = this;

          this.dying = false;
          this.alive = true;

          var pos = positionManager.calcNewPos(options, true),
              bodyData = {
                  x: pos[0],
                  y: pos[1],
                  angularVelocity: Math.random() /1000,
                  restitution: 0.9,
                  cof: 8,
                  vx: this.range(options.vx),
                  vy: this.range(options.vy),
                  treatment: options.static ? 'static' : 'dynamic'
              };

          switch (options.body) {
              case 'circle':
                  bodyData.radius = positionManager.getAbsDim(options.radius/2);

                  bodyData.styles = {
                      src:    options.src,
                      width:  bodyData.radius*2,
                      height: bodyData.radius*2,
                      mass:   4/3*Math.PI * bodyData.radius * bodyData.radius * bodyData.radius
                  };

                  break;

              case 'rectangle':
                  bodyData.width  = options.width;//  * sceneSize;
                  bodyData.height = options.height;// * sceneSize;
                  bodyData.styles = {
                      src:    options.src,
                      width:  bodyData.width,
                      height: bodyData.height,
                      mass:   bodyData.width * bodyData.width * bodyData.height
                  };
          }

          this.body = Physics.body(options.body, bodyData);

          world.add(this.body);

          if (options.blend != 0) {

              var waitInterval = setInterval(function () {
                  if (that.body && that.body.view) {
                      that.body.view.blendMode = options.blend;
                      clearInterval(waitInterval);
                  }
              }, 10);
          }
      };

      this.range = function (v) {
          if (!v) return 0;
          if (v.constructor === Array) {
              var a = Math.min(v[0], v[1]),
                  b = Math.max(v[1], v[1]);

              return Math.random() * (b - a) + a;
          }
          return v;
      };

      this.suicide = function () {
          this.dying = true;
      };

      this.doStep = function () {
          if (this.dying) {
              this.body.view.alpha *= 0.9;;
              if (this.body.view.alpha  < 0.1) {
                  world.remove(this.body);
                  this.alive = false;
              }
          }
      };

      this.init();
  };
})