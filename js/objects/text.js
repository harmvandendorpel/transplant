define([], function () {
  return function (options, $node, positionManager) {
    "use strict";

    this.init = function () {
      this.alive = true;
      this.dying = false;

      var pos = positionManager.calcNewPos(options);

      this.$text = $('<div></div>').text(options.text).addClass('text');

      $node.prepend(this.$text);

      /* var x = Math.max(pos[0] * 2 , $newBody.width()  / 2),
       y = Math.max(pos[1] * 2 , $newBody.height() / 2);

       x = Math.min(x, stageDim[0]*2 - $newBody.width()  / 2);
       y = Math.min(y, stageDim[1]*2 - $newBody.height() / 2);*/

      var x = pos[0],// - this.$text.width()  / 2,
        y = pos[1];// - this.$text.height() / 2;

      this.$text.css(
        'transform',
        'translate3d(' + x + 'px,' + y + 'px,0px)'
      );
    };

    this.suicide = function () {
      var that = this;
      this.$text.fadeOut(function () {
        that.$text.remove();
        that.alive = false;
      });
    };

    this.doStep = function () {

    };

    this.init();
  }
});
