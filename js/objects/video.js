define([], function () {
 return function (options, renderer, world, positionManager) {
  "use strict";

  this.init = function () {
    var that = this;
    this.alive = true;
    this.dying = false;
    var path = process.cwd();
    var directPath = 'file://' + path + '/' + options.src;
    this.player = document.getElementById('videoPlayer');
    this.player.style.visibility = "hidden";

    this.player.play(directPath);
    this.player.audio.mute = true;
    //this.player.video.crop = true;

    if (options.randomstart || options.random) {
      this.player.position = Math.random();
    }
    setTimeout(function () {
      that.player.style.visibility = "visible";
    },1000);
  };

  this.suicide = function () {
    this.player.style.visibility = "hidden";
    this.player.stop();
    this.alive = false;
  };

  this.doStep = function () {

    if (options.random && Math.random() > 0.995) {
      this.player.position = Math.random();
    }
  };



  this.init();
}});
