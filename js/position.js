define([], function () {
  return function (stageDim) {

    var sceneSize = Math.min(stageDim[0], stageDim[1]),
      sceneDefTranslate = [
        (stageDim[0] - sceneSize) / 2,
        (stageDim[1] - sceneSize) / 2
      ],
      prevPos = null;

    this.getAbsDim = function (s) {
      return sceneSize * s/2;
    };

    this.calcNewPos = function(options, adjustRatio) {

      var result = null,
        pos  = options.pos,
        dpos = options.dpos;

      if (pos) {
        result = [
          (pos[0]  * sceneSize + sceneDefTranslate[0]) ,
          (pos[1]  * sceneSize + sceneDefTranslate[1])
        ];
        this.prevPos = pos;
      } else {
        if (dpos && this.prevPos) {
          this.prevPos = [
            this.prevPos[0] + dpos[0],
            this.prevPos[1] + dpos[1]
          ];
          result = [
            (this.prevPos[0] )* sceneSize + sceneDefTranslate[0] ,
            (this.prevPos[1] )* sceneSize + sceneDefTranslate[1]
          ];
        } else {
          result = [
            stageDim[0] * Math.random(),
            stageDim[1] * Math.random()
          ];
        }
      }

      return [
        result[0] + (Math.random()/2 - 0.25),
        result[1] + (Math.random()/2 - 0.25)
      ];
    };

  }
});
