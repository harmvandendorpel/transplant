
define([], function (){

  return {
    "ADD_BODY"          : "addBody",
    "REMOVEBODY"        : "removeBody",
    "WAIT"              : "wait",
    "ADD_TEXT"          : "addText",
    "ADD_DELAUNAY"      : "addDelaunay",
    "ADD_LSYSTEM"       : "addLSystem",
    "ADD_BACKGROUND"    : "addBackground",
    "HANDWRITING"       : "HANDWRITING",
    "VIDEO": 'video'
  };
});

