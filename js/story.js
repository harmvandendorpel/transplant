define([
  './stories/master',
  './step-kinds',
  './objects/body',
  './objects/delaunay',
  './objects/background-image',
  './objects/handwriter',
  './objects/lsystem',
  './objects/text',
  './objects/video'

], function (
  master,
  StepKind,
  BodyObject,
  DelaunayObject,
  BackgroundImageObject,
  HandwriterObject,
  LSystemObject,
  TextObject,
  VideoObject
) {

  return function (renderer, world, Physics, $node, positionManager) {
    var programIndex = 0;
    var steps = null;

    var intervalId = null;
    var stepIndex = 0;//parseInt(Math.random() * steps.length),
    var waiting = 0;
    var stepDuration = 1000;

    this.setSteps = function () {
      console.log('Play program ' + programIndex);
      stepIndex = 0;
      steps = master;
      waiting = 0;
      this.requestSuicide(true);
    };

    this.registerObject = function (object, TTL, kind) {
        window.objectsOnStage.push({
            object: object,
            TTL: TTL || 1,
            kind: kind
        });
    };

    this.requestSuicide = function (force) {
      var result = [];

      $.each(window.objectsOnStage, function (i, object){

        if (force) {
          object.TTL = Math.min(object.TTL, 20);
        }

        if (!object.object.alive) {
            // don't do anything, this object will not be passed on to the next round
        } else {
          if (object.TTL > 0) {
              object.TTL--;
          } else {
              object.object.suicide();
          }
          result.push(object);
        }
      });

      window.objectsOnStage = result;
    };

    this.updateAllObject = function () {
      for (var i=0; i < window.objectsOnStage.length; i++) {
        var object =  window.objectsOnStage[i].object;
        object.doStep();
      }
    };

    this.doStoryStep = function () {
      this.requestSuicide()  ;

      if (waiting > 0) {
        waiting--;
        return;
      }

      if (stepIndex >= steps.length) {
        stepIndex = 0;
      }

      var options = steps[stepIndex];
      var count = options.count || 1;

      var $layer = $('#layer' + (options.layer || 1));

      for (var i = 0; i < count;i++) {
        switch (options.kind) {

          case StepKind.HANDWRITING:
            this.registerObject(
              new HandwriterObject(options, renderer, world, positionManager),
              options.TTL,
              options.kind
            );
            break;

          case StepKind.ADD_BODY:
            this.registerObject(
              new BodyObject(options, world, Physics, positionManager),
              options.TTL,
              options.kind
            );
            break;

          case StepKind.VIDEO:
            this.registerObject(
              new VideoObject(options, world, Physics, positionManager),
              options.TTL,
              options.kind
            );
            break;

          case StepKind.ADD_TEXT:
            var that = this;
            setTimeout(function () {
              that.registerObject(
                new TextObject(options, $layer, positionManager),
                options.TTL,
                options.kind
              );
            }, i * 250);
            break;

          case StepKind.ADD_DELAUNAY:
            this.registerObject(
              new DelaunayObject(options, $layer, positionManager),
              options.TTL,
              options.kind
            );
            break;

          case StepKind.ADD_LSYSTEM:
            this.registerObject(
                new LSystemObject(options, $layer, positionManager),
                    options.TTL,
                    options.kind
            );
            break;

          case StepKind.ADD_BACKGROUND:
            this.registerObject(
                new BackgroundImageObject(options, renderer, world, positionManager),
                options.TTL,
                options.kind
            );
            break;

          case StepKind.WAIT:
            waiting = options.steps;
            break;

          default:
            console.error('Unknow step ' + options.kind);
        }
      }

      stepIndex++;
      if (stepIndex > steps.length) {
        /*programIndex++;
        if (programIndex >= master.length) {
            programIndex = 0;
        }*/
        this.setSteps();
      }
    };

    this.doStep = function () {
      this.updateAllObject();
    };

    this.begin = function () {
      this.setSteps();
      var that = this;
      intervalId = setInterval($.proxy(this.doStoryStep, this), stepDuration);

      $(document).keyup(function(e) {
        var keyCode = e.keyCode,
            keyCodeOneKey = 49;
        if (keyCode >= keyCodeOneKey && keyCode <= keyCodeOneKey + 9) {
            var newProgramIndex = keyCode - keyCodeOneKey;
            if (newProgramIndex <= master.length - 1) {
                programIndex = newProgramIndex;
                that.setSteps();
            }
        }
      });
    };
  };

});